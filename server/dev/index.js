import path from 'path';
import open from 'open';
import express from 'express';
import webpack from 'webpack';
import config from '../../webpack.dev.config.babel';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

const app = express(), compiler = webpack(config);
const PORT = process.env.PORT || 4000;
//Expose public folder
app.use(express.static(path.resolve(__dirname, '../../public')));

//Watch all chaanges in react files
app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
    watchOptions: {
        poll: true
    }
}));
app.use(webpackHotMiddleware(compiler, {
    watchOptions: {
        poll: true
    }
}));
//Send all traffict to react
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../public', 'index.html'));
});

app.listen(PORT, (error) => {
    if (!error) {
        open('http://localhost:${port}');
    }
});