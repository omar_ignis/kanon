//Dependencies
import React from 'react';
import { Route, Switch } from 'react-router-dom';

//Components
import App from './containers/app';
// import Item from './containers/item';
// import Warehouse from './containers/warehouse';

const Routes = () =>
    <App>
        <Switch>
            {/* <Route exact path="/item" component={Item} />
            <Route exact path="/warehouse" component={Warehouse} /> */}
        </Switch>
    </App>
export default Routes;