//Dependecies
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './routes';
//Assets
//import './index.css';

render(
    <Router>
        <Routes />
    </Router>,
    document.getElementById('root')
    );