import path from 'path';
import webpack from 'webpack';

export default {
    entry: {
        index: [
            path.join(__dirname, './src/index'),
            'webpack-hot-middleware/client?reload=true'
        ]
    },
    output: {
        path: path.join(__dirname, 'public/dist'),
        filename: 'dist/[name].bundle.js',
        publicPath: '/'
    },
    target: 'web',
    mode: 'development',
    devtool: '#source-map',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.html$/,
                use: { loader: 'html-loader' }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(gif|png|woff|woff2|eot|ttf|svg)$/,
                use: ['url-loader?limit=100000']
            }
        ]
    },
    plugins:[
        // Esto es para la version de produccion
        // new HtmlWebPackPlugin({
        //     template: path.join(__dirname, 'src/templates/index.html'),
        //     filename: path.join(__dirname, 'public/index.html')

        // }),
        new webpack.HotModuleReplacementPlugin(),
    ]
}